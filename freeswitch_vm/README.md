building freeswitch from source
===============================

ref: https://freeswitch.org/confluence/display/FREESWITCH/Debian+8+Jessie




install openresty
=================

see: http://www.linuxsecrets.com/blog/9web-server-howto-and-installation/2015/08/21/1631-complete-guide-installing-openresty-a-nginx-full-fledged-web-server-on-redhat-scientific-linux-debian

# install openresty
    sudo apt-get install -y libreadline-dev libncurses5-dev libpcre3-dev libssl-dev perl make build-essential 

    cd /usr/src
    sudo wget https://openresty.org/download/ngx_openresty-1.9.3.1.tar.gz
    sudo tar zxvf ngx_openresty-1.9.3.1.tar.gz
    cd ngx_openresty-1.9.3.1
    sudo ./configure

    sudo make
    sudo make install

# install luarocks
    cd /usr/src
    sudo wget https://codeload.github.com/keplerproject/luarocks/tar.gz/v2.0.13 -O luarocks-2.0.13.tar.gz

    sudo tar -xzvf luarocks-2.0.13.tar.gz
    cd luarocks-2.0.13/
    sudo ./configure --prefix=/usr/local/openresty/luajit --with-lua=/usr/local/openresty/luajit/ --lua-suffix=jit-2.1.0-alpha --with-lua-include=/usr/local/openresty/luajit/include/luajit-2.1
	sudo make
    sudo make install

	sudo ln -s /usr/local/openresty/luajit/bin/luarocks-5.1 /usr/local/bin/luarocks

	# usage 
	# sudo luarocks install redis-lua


 # set up lua

     sudo ln -s /usr/local/openresty/luajit/bin/luajit-2.1.0-alpha /usr/local/bin/lua

     sudo mkdir /usr/local/share/lua
     sudo ln -s /usr/local/openresty/lualib/ /usr/local/share/lua/5.1  

