#!/usr/bin/env bash
sudo su

apt-get update && apt-get install -y curl

#
# prerequisites
#
#  files in /tmp 
#      - mod_lua_1.2.tar.gz
#      - conf.zip
#      - scripts.zip


#
# install openresty
#
cd /usr/src
wget http://openresty.org/download/ngx_openresty-1.7.10.1.tar.gz \
    -O ngx_openresty-1.7.10.1.tar.gz
tar xzvf ngx_openresty-1.7.10.1.tar.gz
cd ngx_openresty-1.7.10.1
./configure
make
sudo make install
# add lua to PATH
sudo ln -s /usr/local/openresty/luajit/bin/luajit-2.1.0-alpha /usr/local/bin/lua

#
# install luarocks
#
cd /usr/src
wget https://codeload.github.com/keplerproject/luarocks/tar.gz/v2.0.13 \
    -O luarocks-2.0.13.tar.gz
tar -xzvf luarocks-2.0.13.tar.gz
cd luarocks-2.0.13/
./configure --prefix=/usr/local/openresty/luajit \
    --with-lua=/usr/local/openresty/luajit/ \
    --lua-suffix=jit-2.1.0-alpha \
    --with-lua-include=/usr/local/openresty/luajit/include/luajit-2.1
make
sudo make install
# add luarocks to PATH
sudo ln -s /usr/local/openresty/luajit/bin/luarocks-5.1 /usr/local/bin/luarocks


# get lua 5.1 sources
cd /usr/src && wget http://www.lua.org/ftp/lua-5.1.5.tar.gz &&  tar xvf lua-5.1.5.tar.gz
#ls /usr/src/lua-5.1.5/src


# install freeswitch dependencies
curl https://files.freeswitch.org/repo/deb/debian/freeswitch_archive_g0.pub | apt-key add -
echo "deb http://files.freeswitch.org/repo/deb/freeswitch-1.6/ jessie main" > /etc/apt/sources.list.d/freeswitch.list
apt-get update && apt-get install -y --force-yes --fix-missing freeswitch-video-deps-most

# get freeswitch sources
git config --global pull.rebase true
cd /usr/src/ && git clone -b v1.6 https://freeswitch.org/stash/scm/fs/freeswitch.git freeswitch.git


# replace lua 5.2 with lua 5.1 in freeswitch  ( mod_lua from freeswitch 1.2 )
#mv /usr/src/freeswitch.git/src/mod/languages/mod_lua/lua /usr/src/freeswitch.git/src/mod/languages/mod_lua/lua.5.2
#ln -s  /usr/src/lua-5.1.5/src /usr/src/freeswitch.git/src/mod/languages/mod_lua/lua

cd /usr/src/freeswitch.git/src/mod/languages/ && mv mod_lua mod_lua_1.6 && tar xvf /tmp/mod_lua_1.2.tar.gz ./



# make freeswitch with lua 5.1
cd /usr/src/freeswitch.git
./bootstrap.sh -j
./configure
make
make install

# install freeswitch sounds
apt-get install -y freeswitch-sounds-en-us-callie
sudo rm -r /usr/local/freeswitch/sounds
sudo ln -s /usr/share/freeswitch/sounds /usr/local/freeswitch/sounds



# install some luarocks
sudo luarocks install redis-lua
sudo luarocks install uuid



# Enjoy luarocks as: 
# sudo /usr/local/openresty/luajit/luarocks install lapis

# install redis server
sudo apt-get install -y redis-server


# transfer freeswitch conf
sudo mv /usr/local/freeswitch/conf /usr/local/freeswitch/conf.bak
cd /tmp
unzip conf.zip
sudo mv ./conf /usr/local/freeswitch/conf



# start freeswitch 
cd /usr/local/freeswitch/
sudo ./bin/freeswitch -ncwait -nonat
#./bin/fs_cli
