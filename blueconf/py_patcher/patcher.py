
"""

    apply patch on a file system according to a patchs.py file


patch.py format
---------------

PATCH_LIST = [

    {   $$file_1   },
    {   $$file_2   },

]


$$file format

{
    'name': 'vars.xml',
    'file':  "../freeswitch_conf/vanilla/vars.xml",
    'patchs': [

        { $$patch_1 },
        { $$patch_2 }
    ]
}


$$patch_format

{
  'lines':   '<X-PRE-PROCESS cmd="set" data="default_password=%(PASSWORD)se"/>',
  'check':   '<X-PRE-PROCESS cmd="set" data="default_password=%(PASSWORD)s"/>',
  'anchor':  '<X-PRE-PROCESS cmd="set" data="default_password=1234"/>',
  'action': "replace",

}

lines: string , the line(s) to insert in file
check: string  (optional default to lines) , if match : skip  the patch
anchor: string ( optional ) the line to match to locate point of insertion , or BOF , EOF default to EOF
action: string : (optional) [ replace,insert_after,insert_before ] default to insert_after

lines,check,anchor can be regex if prefixed with "re:"


"""
import os
import re

from patchs import *

#import ConfigParser
#from ConfigParser import ConfigParser, SafeConfigParser
import logging

log= logging.getLogger('patcher')



def regex(text,context=None):
    """


    :param text:
    :return:
    """
    # search anchor
    if text.startswith('re:'):
        # text is a regex
        text= text[3:]
        rx = re.compile(text)
    else:
        # not a regex
        rx= re.compile(re.escape(text))

    # return the compiled regex
    return rx




def job(dry=False):
    """


    :param dry: boolean, true: save ref file with .bak , False generate patch as .patch
    :return:
    """


    actions= ['replace','insert_before','insert_after']
    anchors= [ 'EOF', 'BOF']

    log.info('begin')

    #log.info( "patch list: %s" % str(cfg.sections()))


    #main=cfg._sections['main']

    # for each file in path list
    for file_data in PATCH_LIST:

        #if section == "main":
        #    continue
        base= '.'
        log.info('<=== apply patches: [%s]' % file_data['name'])

        file= os.path.join(base,file_data['file'])
        log.info('file = [%s]' % file)

        # check file exists
        try:
            os.stat(file)
        except:
            log.error('cannot read file: ABORT')

        file_content= open(file,'r').read()

        final_text= file_content

        # for each patch on this file
        for patch_data in file_data['patchs']:

            #  lines to add or replace
            text= patch_data['lines'] % globals()

            # search for check (default check is lines)
            check = patch_data.get('check',text) % globals()
            check= re.compile(check,re.MULTILINE)

            m= check.search(final_text)

            if m:
                log.warning('patch already applied. ===> PATCH SKIP')
                continue

            # search anchor
            anchor_text= patch_data.get('anchor',"EOF")

            if anchor_text== "EOF":
                log.info('insert at End Of File')
                final_text= file_data + '\n' + text + '\n'
            elif anchor_text == 'BOF':
                log.info('insert at Beginning Of File')
                final_text= text + '\n' + file_data + '\n'

            else:
                # search anchor
                anchor= regex(anchor_text)

                log.info("search for anchor: [%s]" % anchor_text)
                m= anchor.search(final_text)
                if not m:
                    log.error("cannot find anchor.  ===> PATCH ABORT" )
                    continue

                # anchor found
                regs= m.regs

                if not len(regs) ==1 :
                    log.error("more than one anchor found. ===> PATCH ABORT")
                    continue

                else:
                    # we found the anchor
                    log.info("anchor found")
                    start= regs[0][0]
                    end= regs[0][1]
                    first=  final_text[:start]
                    line= final_text[start:end]
                    last= final_text[end:]

                    #print first, line , last

                    action= patch_data.get('action','insert_after')

                    if not action in actions:
                        log.error('action must be in : %s' , str(actions))
                    if action == 'insert_before':
                        # insert before
                        log.info('insert before anchor')
                        final_text= first + '\n' +  text + '\n' + line + '\n' + last

                    elif action == 'replace':
                        log.info('replace line')
                        final_text= first + '\n' +  text + '\n' + last
                    else:
                        # insert after
                        log.info('insert after anchor')
                        final_text= first + line + '\n' + text + '\n' + last

                # write final text
                if dry:
                    # leave source file unchanged, generate a .patch file
                    with open(file + ".patch","w") as fh:
                        fh.write(final_text)
                        log.info("===> DRY PATCH DONE.")
                else:
                    # apply patch, save origin as .bak
                    with open(file ,"r") as fr:
                        with open(file + ".bak", "w") as fw:
                            fw.write(fr.read())
                        log.debug("save original file as .bak")
                    with open(file,"w") as fh:
                        fh.write(final_text)
                        log.info("===> PATCH DONE.")


        continue


    log.info('end')


if __name__=='__main__':

    logging.basicConfig(level=logging.DEBUG)

    #job(dry=True)

    job()

    print "Done."