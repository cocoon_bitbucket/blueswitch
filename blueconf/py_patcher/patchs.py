

HOST_IP="192.168.1.51"

PASSWORD="1234"

RTP_START_PORT="16385"
RTP_END_PORT="16394"

REDIS_IP="127.0.0.1"
REDIS_PORT="6379"

BASE_FREESWITCH="../../freeswitch_conf/conf"


PATCH_LIST =[


{
'name': 'vars.xml',
'file':  "%s/vars.xml" % BASE_FREESWITCH,
'patchs': [

		{
          'lines':   '<X-PRE-PROCESS cmd="set" data="default_password=%(PASSWORD)s"/>',
          'check':   '<X-PRE-PROCESS cmd="set" data="default_password=%(PASSWORD)s"/>',
          'anchor':  '<X-PRE-PROCESS cmd="set" data="default_password=1234"/>',
          'action': "replace",
		},

        {
          'lines':   '''\
<X-PRE-PROCESS cmd="set" data="host_ip_v4=%(HOST_IP)s"/>
<X-PRE-PROCESS cmd="set" data="internal_rtp_ip=%(HOST_IP)s"/>
<X-PRE-PROCESS cmd="set" data="internal_sip_ip=%(HOST_IP)s"/>
''',

          'anchor':  '<X-PRE-PROCESS cmd="set" data="domain=$${local_ip_v4}"/>',
          'action': "insert_after",
        },
		{
          'lines':   '<X-PRE-PROCESS cmd="set" data="external_rtp_ip=%(HOST_IP)s"/>',
          'anchor':  '<X-PRE-PROCESS cmd="set" data="external_rtp_ip=stun:stun.freeswitch.org"/>',
          'action': "replace",
		},
		{
          'lines':   '<X-PRE-PROCESS cmd="set" data="external_sip_ip=%(HOST_IP)s"/>',
          'anchor':  '<X-PRE-PROCESS cmd="set" data="external_sip_ip=stun:stun.freeswitch.org"/>',
          'action': "replace",
		},
]
},
{
'name': 'internal.xml',
'file':  "%s/sip_profiles/internal.xml" % BASE_FREESWITCH,
'patchs': [
		{
          'lines':   '<param name="rtp-ip" value="$${internal_rtp_ip}"/>',
          'anchor':  '<param name="rtp-ip" value="$${local_ip_v4}"/>',
          'action': "replace",
		},
		{
          'lines':   '<param name="sip-ip" value="$${internal_sip_ip}"/>',
          'anchor':  '<param name="sip-ip" value="$${local_ip_v4}"/>',
          'action': "replace",
		},
		{
          'lines':   '<param name="ext-rtp-ip" value="$${external_rtp_ip}"/>',
          'anchor':  '<param name="ext-rtp-ip" value="auto-nat"/>',
          'action': "replace",
		},
		{
          'lines':   '<param name="ext-sip-ip" value="$${external_sip_ip}"/>',
          'anchor':  '<param name="ext-sip-ip" value="auto-nat"/>',
          'action': "replace",
		},
]
},
{
'name': 'external.xml',
'file':  "%s/sip_profiles/external.xml" % BASE_FREESWITCH,
'patchs': [
		{
          'lines':   '<param name="rtp-ip" value="$${internal_rtp_ip}"/>',
          'anchor':  '<param name="rtp-ip" value="$${local_ip_v4}"/>',
          'action': "replace",
		},
		{
          'lines':   '<param name="sip-ip" value="$${internal_sip_ip}"/>',
          'anchor':  '<param name="sip-ip" value="$${local_ip_v4}"/>',
          'action': "replace",
		},
		{
          'lines':   '<param name="ext-rtp-ip" value="$${external_rtp_ip}"/>',
          'anchor':  '<param name="ext-rtp-ip" value="auto-nat"/>',
          'action': "replace",
		},
		{
          'lines':   '<param name="ext-sip-ip" value="$${external_sip_ip}"/>',
          'anchor':  '<param name="ext-sip-ip" value="auto-nat"/>',
          'action': "replace",
		},
]
},

{
'name': 'dialplan/default.xml',
'file':  "%s/dialplan/default.xml" % BASE_FREESWITCH,
'patchs': [
		{
          'lines':   '<condition field="destination_number" expression="^(19[01][0-9])$">',
          'anchor':  '<condition field="destination_number" expression="^(11[01][0-9])$">',
          'action': "replace",
		},
]
},

{
'name': 'lua.conf.xml',
'file':  "%s/autoload_configs/lua.conf.xml" % BASE_FREESWITCH,
'patchs': [
		{
          'lines':   '''
<param name="script-directory" value="$${script_dir}/?.lua"/>
<param name="script-directory" value="/usr/local/openresty/luajit/share/lua/5.1/?.lua"/>
<param name="script-directory" value="/usr/local/openresty/luajit/share/lua/5.1/socket/?.lua"/>

<param name="script-directory" value="$${script_dir}/?.so"/>
<param name="module-directory" value="/usr/local/openresty/luajit/lib/lua/5.1/?.so"/>
<param name="module-directory" value="/usr/local/openresty/luajit/lib/lua/5.1/socket/?.so"/>
<param name="module-directory" value="/usr/local/openresty/luajit/lib/lua/5.1/mime/?.so"/>
''',
          'anchor':  '<settings>',
          'action': "insert_after",
		},
]
},
{
'name': 'switch.conf.xml',
'file':  "%s/autoload_configs/switch.conf.xml" % BASE_FREESWITCH,
'patchs': [
		{
          'lines':   '<param name="rtp-start-port" value="%(RTP_START_PORT)s"/>',
          'anchor':  '<!-- <param name="rtp-start-port" value="16384"/> -->',
          'action': "replace",
		},
		{
          'lines':   '<param name="rtp-end-port" value="%(RTP_END_PORT)s"/>',
          'anchor':  '<!-- <param name="rtp-end-port" value="32768"/> -->',
          'action': "replace",
		},
]
},


]






