"""


	config.py


"""

base_template= 		"./templates"
base_freeswitch=    "/usr/local/freeswitch/conf"
#base_freeswitch= 	"/etc/freeswitch"


default_vars= {

   # in vars.xml
	"default_password": "1234",
    "external_rtp_ip": "192.168.99.100",
    "external_sip_ip": "192.168.99.100",

    # in autoload_configs/switch.conf.xml
    "rtp_start_port": "16384",
    "rtp_end_port":   "16394",
}

default_dockerfile_vars={
	"base_image": "cocoon/base",
	"volumes": [],
	"environment": [],
	"expose": [],
	"add":[],
	"run":[],
	"provisioner": False,
	"entrypoint": "",
	"command": ""
}


paths= {
	
	"vars":        		"vars.xml",
	"internal":    		"sip_profiles/internal.xml",
	"external":    		"sip_profiles/external.xml",
    "switch.conf": 		"autoload_configs/switch.conf.xml",
    "lua.conf":			"lua.conf.xml",
    "default":     		"dialplan/default.xml",

    "bluebox":          "dialplan/default/bluebox.xml"
}


users= [

    dict( user_id= '+33146511101',
          accountcode='+33146511101',
          effective_caller_id_name='Aline',
          effective_caller_id_number='33146511101'),

    dict( id= '+33146511102', name= 'Bruce'),
    dict( id= '+33146511103', name='Charlene')
]



###   utilities

def template_filename(name,base_template=base_template):
	"""

	"""
	return "%s/%s.xml.j2" % (base_template,name)

def destination_filename(name,root= base_freeswitch):
	"""

	"""
	return "%s/conf/%s" % (root,paths[name])
