__author__ = 'cocoon'


open_mark= "+++ "
comment_mark= "--- "



class Writer(object):
    """


    """
    def __init__(self,name,verbose=True):
        """

        :param name:
        :return:
        """
        self._name=name
        self._verbose= verbose

        if self._verbose:
             print "OPEN %s" % self._name

    def close(self):
        """
        """
        if self._verbose:
            print "CLOSE %s" % self._name

    def write(self,line):
        """
        """
        print ">>> " , line




class FileWriter(Writer):
    """

    """
    def __init__(self,line,verbose=True):
        """

        :param line:
        :return:
        """
        super(FileWriter).__init__(line,verbose)
        self._fh= open(self._name,"wt")

    def close(self):
        """

        :return:
        """
        super(FileWriter).close()
        self._fh.close()

    def write(self,line):
        """

        :param line:
        :return:
        """
        self._fh.write(line)


def deploy_conf(filename,writer_class=Writer):
    """

        read file
            - ignore line like --- *
            - open file when encounters +++ $filename
            - write in file

    :param filename:
    :return:
    """

    content= file(filename,"rt")
    status= "off"
    current=None

    for line in content:

        if line.startswith(comment_mark):
            # ignore line
            print line
            continue

        if line.startswith(open_mark):
           # detect a line beginning with +++ $filename
           target_filename= line[len(open_mark):].strip()

           if status == "off":
                #current=open( target_filename,"wb")
                current= writer_class(target_filename)
                #print "OPEN %s" % target_filename
                status= "on"
                continue
           elif status == "on":
                # close current file if any
                if current:
                    current.close()
                    current= None
                #print "CLOSE "
                #print "OPEN %s" % target_filename
                #current=open( target_filename,"wb")
                current= writer_class(target_filename)
                continue
        else:
            # write line in file if any
            if current:
                #current.write(line)
                #print "write line %s" % line
                current.write(line)

    if current:
        current.close()
        print "END"
    return
