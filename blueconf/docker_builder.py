__author__ = 'cocoon'
"""


    base image for data container is derived from busybox

    FROM progrium/busybox
    MAINTAINER Yaser Martinez Palenzuela <@elyase>
    RUN opkg-install bzip2 libsqlite3 libpthread zlib libopenssl
    ADD pyrun2.7 /bin/python
    RUN ln -s /usr/lib/libbz2.so.1.0 /usr/lib/libbz2.so.1
    RUN ln -sf /lib/libpthread-2.18.so /lib/libpthread.so.0



"""

#docker_server= "tcp://192.168.99.100:2376"
docker_server= "https://192.168.99.100:2376"
#docker_server= "unix://var/run/docker.sock"


from dockermap.api import DockerClientWrapper, DockerFile

client = DockerClientWrapper(docker_server)


df = DockerFile(
    u"progrium/busybox",
    maintainer= u"cocoon, cocoon.project@gmail.com",
    )

df.run("opkg-install bzip2 libsqlite3 libpthread zlib libopenssl")


client.build_from_file(df, 'new_image')