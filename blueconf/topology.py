__author__ = 'cocoon'


from tempita import Template

import config as cnf

from bluebox_directory import gen_directory
from deploy import deploy_conf,open_mark, comment_mark



def iter_conf(vars,templates='./templates'):
    """

    :param vars: dict , values of vars
    :param templates: string : base directory for templates
    :return: yield name, target_filename, content
    """
    for name in cnf.paths.keys():

        target_filename= cnf.destination_filename(name)
        content= file(cnf.template_filename(name)).read()
        t= Template(content)

        target_content= t.substitute (vars)

        yield name, target_filename, target_content


def gen_conf(vars,templates='./templates'):


    # generate conf for cnf.paths
    for name,filename,content in iter_conf(vars,):

        print comment_mark  + name      # "--- "
        print open_mark  + filename     # "+++ "
        print content

    # generate file for bluebox directory
    gen_directory(cnf.users,cnf.base_freeswitch)


    return



if __name__=="__main__":



    def test_basic():

        vars = cnf.default_vars

        # in vars.xml
        vars['default_password']="1357"
        vars["external_rtp_ip"]= "192.168.99.100"
        vars["external_sip_ip"]= "192.168.99.100"

        # in autoload_configs/switch.conf.xml
        vars["rtp_start_port"]= "16384"
        vars["rtp_end_port"]=   "16394"


        for name in cnf.paths.keys():


            content= file(cnf.template_filename(name)).read()

            t= Template(content)

            target= t.substitute (vars)
            print target

            # check targets
            if name == 'switch.conf':
                assert '<param name="rtp-start-port" value="16384"/>' in target
                assert '<param name="rtp-end-port"   value="16394"/>' in target
            elif name == 'vars':
                assert "default_password=1357" in target
                assert "external_rtp_ip=192.168.99.100" in  target
                assert "external_sip_ip=192.168.99.100" in  target



        return



    def test_iter_conf():

        vars = cnf.default_vars

        # in vars.xml
        vars["external_rtp_ip"]= "192.168.99.100"
        vars["external_sip_ip"]= "192.168.99.100"

        # in autoload_configs/switch.conf.xml
        vars["rtp_start_port"]= "16384"
        vars["rtp_end_port"]=   "16394"


        for name,filename,content in iter_conf(vars,):

            print "--- "  + name
            print "+++ "   + filename
            print content
            print

            if name == 'switch.conf':
                assert "16384" in content
                assert "16394" in content
            elif name == 'vars':
                assert "192.168.99.100" in  content

        return

    def test_gen_conf():

        vars = cnf.default_vars

        # in vars.xml
        vars["external_rtp_ip"]= "192.168.99.100"
        vars["external_sip_ip"]= "192.168.99.100"

        # in autoload_configs/switch.conf.xml
        vars["rtp_start_port"]= "16384"
        vars["rtp_end_port"]=   "16394"

        gen_conf(vars)



    def test_gen_dockerfile():
        """


        :return:
        """
        context= cnf.default_dockerfile_vars
        context.update({
            "base_image": "cocoon/base",
            "volumes": [],
            "environment": ["here","there"],
            "expose": [],
            "add":[],
            "run":[],
            "provisioner": True,
            "entrypoint": "entry point",
            "command": "ls -l"
        })

        content= file("./templates/dockerfile.tmpl").read()

        t= Template(content)

        target= t.substitute (context)

        print target

        return

    def test_deploy_conf():
        """


        :return:
        """
        deploy_filename= "./samples/deploy.txt"

        deploy_conf(deploy_filename)
        return



    #
    test_basic()

    test_iter_conf()

    test_gen_conf()
    test_deploy_conf()

    #test_gen_dockerfile()




    pass
