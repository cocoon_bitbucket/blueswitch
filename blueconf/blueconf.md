blueconf
--------


a module to generate freeswitch configuration files




blueswitch configuration image
==============================

a configuration image for freeswith to test syprunner application




freeswitch configuration

conf/vars.xml
=============

add:
<X-PRE-PROCESS cmd="set" data="host_ip_v4=192.168.1.51"/>

customize: 
- <X-PRE-PROCESS cmd="set" data="external_rtp_ip=$${host_ip_v4}"/>
- <X-PRE-PROCESS cmd="set" data="external_sip_ip=$${host_ip_v4}"/>


conf/sip_profiles/internal.xml
===============================
- <param name="rtp-ip" value="$${host_ip_v4}"/>
- <param name="sip-ip" value="$${host_ip_v4}"/>
- <param name="ext-rtp-ip" value="$${external_rtp_ip}"/>
- <param name="ext-sip-ip" value="$${external_sip_ip}"/>


conf/sip_profiles/external.xml
===============================
- <param name="rtp-ip" value="$${host_ip_v4}"/>
- <param name="sip-ip" value="$${host_ip_v4}"/>
- <param name="ext-rtp-ip" value="$${host_ip_v4}"/>
- <param name="ext-sip-ip" value="$${host_ip_v4}"/>


conf/autoload_configs/switch.conf.xml
======================================
-   <!-- RTP port range -->
-    <param name="rtp-start-port" value="16384"/> 
-    <param name="rtp-end-port"   value="16394"/> 


conf/dialplan/default.xml
=========================
 exclude the  Local_Extension_Skinny because of extension collision with 11?? bluebox extensions




notes
=====


problemwith vm:
---------------
check with 

    netstat -vaun





Data container
==============

