__author__ = 'cocoon'
"""

    handle a directory for freeswitch


    generate /etc/freeswitch/conf/directory/default/bluebox.xml
    from data like cnf.users

    input sample: see config.users

    output_sample:

<include>

    <user id="+33146511101">
        <params>
          <param name="password" value="$${default_password}"/>
          <param name="vm-password" value="$${default_password}"/>
        </params>
        <variables>
          <variable name="toll_allow" value="domestic,international,local"/>
          <variable name="accountcode" value="+33146511101"/>
          <variable name="user_context" value="default"/>
          <variable name="effective_caller_id_name" value="Aline"/>
          <variable name="effective_caller_id_number" value="+33146511101"/>
          <variable name="outbound_caller_id_name" value="$${outbound_caller_name}"/>
          <variable name="outbound_caller_id_number" value="$${outbound_caller_id}"/>
          <variable name="callgroup" value="techsupport"/>
          <variable name="syp_enterprise" value="e1"/>
          <variable name="syp_site" value="e1s1"/>
        </variables>
    </user>

    <user id="P2361EVQ3_6330">
        <params>
          <param name="password" value="$${default_password}"/>
          <param name="vm-password" value="$${default_password}"/>
        </params>
        <variables>
          <variable name="toll_allow" value="domestic,international,local"/>
          <variable name="accountcode" value="P2361EVQ3_6330"/>
          <variable name="user_context" value="default"/>
          <variable name="effective_caller_id_name" value="Alice_InterEnterprise_noSDA"/>
          <variable name="effective_caller_id_number" value="P2361EVQ3_6330"/>
          <variable name="outbound_caller_id_name" value="$${outbound_caller_name}"/>
          <variable name="outbound_caller_id_number" value="$${outbound_caller_id}"/>
          <variable name="callgroup" value="techsupport"/>
          <variable name="syp_enterprise" value="e1"/>
          <variable name="syp_site" value="e1s1"/>
        </variables>
    </user>

    <user> ... </user>
</include>


"""
from tempita import Template
from deploy import comment_mark,open_mark


user_pattern= {

    'default': {
        'user_id': "$user_id",
        'accountcode': "$accountcode",
        'effective_caller_id_name':   '$effective_caller_id_name',
        'effective_caller_id_number': '$effective_caller_id_number',

        'syp_enterprise': 'e1',
        'syp_site': 'e1s1',

        'user_context': 'default',
        'callgroup':    'techsupport',

    },
    'pattern': """\
    <user id="{{ user_id }}">
        <params>
          <param name="password" value="$${default_password}"/>
          <param name="vm-password" value="$${default_password}"/>
        </params>
        <variables>
          <variable name="toll_allow" value="domestic,international,local"/>
          <variable name="accountcode" value="{{ accountcode }}"/>
          <variable name="user_context" value="{{ user_context }}"/>
          <variable name="effective_caller_id_name" value="{{ effective_caller_id_name }}"/>
          <variable name="effective_caller_id_number" value="{{ effective_caller_id_number }}"/>
          <variable name="outbound_caller_id_name" value="$${outbound_caller_name}"/>
          <variable name="outbound_caller_id_number" value="$${outbound_caller_id}"/>
          <variable name="callgroup" value="{{ callgroup }}"/>
          <variable name="syp_enterprise" value="{{ syp_enterprise }}"/>
          <variable name="syp_site" value="{{ syp_site }}"/>
        </variables>
    </user>
""",
    'destination': "directory/default/bluebox.xml"
}



def get_directory_entry(user_properties):
    """


    :param user: a dict like user_pattern['default']
    :return:
    """
    user= user_properties.copy()


    id= None
    if 'id' in user_properties:
        id= user.pop('id')

    name=None
    if 'name' in user_properties:
        name= user.pop('name')

    data = user_pattern['default'].copy()
    data.update(user)

    if name:
        data['effective_caller_id_name']= name

    if id:
        for field in ['user_id','accountcode','effective_caller_id_number']:
            data[field]=id


    t= Template(user_pattern['pattern'])

    xml_content= t.substitute (data)

    return xml_content


def iter_directory_entry(users_parameters):
    """

    return an xml representation of users

    :param parameters:  list of dict like user_pattern['default']
    :return:
    """

    yield "<include>"

    for entry in users_parameters:

        yield get_directory_entry(entry)

    yield "</include>"



def gen_directory(users,basedir="/etc/freeswitch/conf"):
    """


    :param users:
    :param basedir:
    :return:
    """
    conf= list( iter_directory_entry( users))

    print comment_mark + "bluebox_directory"
    # print open mark: +++ /etc/freesswitch/directory/default/bluebox.xml
    print "%s %s/%s" % (open_mark ,basedir, user_pattern['destination'])
    print "\n".join(conf)



if __name__=="__main__":

    import config as cnf


    sample= [

        dict( user_id= '33146511101', accountcode='33146511101',
              effective_caller_id_name='Aline',effective_caller_id_number='33146511101'),

        dict( user_id= 'P2361EVQ3_6330', accountcode='P2361EVQ3_6330',
              effective_caller_id_name='Alice_InterEnterprise_noSDA',effective_caller_id_number='P2361EVQ3_6330'),

    ]


    def test_basic():

        conf= list(iter_directory_entry(sample))

        conf= list( iter_directory_entry( cnf.users))


        print "\n".join(conf)

        return

    def test_gen_directory():
        """

        :return:
        """
        gen_directory(cnf.users,cnf.base_freeswitch)
        return


    #
    test_basic()

    test_gen_directory()

    pass
