
Overview
========

blueswitch is a voip pbx for test purpose


* based on freeswitch
* implements simple pbx functinalities
* call redirect



Modules
-------

* a vanilla freeswitch
* a lua based config generator
* a generator adapter for syprunner config


packages
--------

* a configurator to inject a configuration to an existing freeswitch
* a vagrant vm 
* a docker-compose file
* a docker image


TODO:
=====

* a fac injector : to inject Feature Access Code to redis
* integrate the dialplan_directory in the setup process




 


