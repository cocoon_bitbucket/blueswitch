README.md

build a freeswtich configuration



get original vanilla config
===========================

    	cd /Users/cocoon/blueswitch

start a vagrant freeswitch vm

	cd freeswitch_vm
	vagrant up
	cd ..


	cd freeswitch_conf
    rm -r vanilla
    scp -r vagrant@192.168.1.51:/usr/src/freeswitch.git/conf/vanilla/ .

now we have freeswitch_cnf/vanilla


prepare a conf
==============

directory: freeswitch_conf

    rm -r conf
    cp -R vanilla conf

now we have freeswitch_cnf/conf


apply patch
===========

directory: blueconf/py_patcher

edit patchs and customize HOST_IP 

    python pather.py


now we have a patched configuration under freeswitch_conf/conf



copy additional files
=====================

directory: freeswitch_conf

    cp bluebox_dialplan.xml conf/dialplan/default/bluebox_dialplan.xml  
    cp bluebox_directory.xml conf/directory/default/bluebox_directory.xml  


scripts
=======



make zip
========


zip blueswitch_conf/conf into blueswitch_conf/deploy/conf.zip


zip blueswitch_conf/scripts into blueswitch_conf/deploy/scripts.zip














