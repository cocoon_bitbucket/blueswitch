--
-- Created by IntelliJ IDEA.
-- User: cocoon
-- Date: 19/02/16
-- Time: 15:52
-- To change this template use File | Settings | File Templates.
--
require("blueswitch_cfg")

local redis_server = REDIS_SERVER or "127.0.0.1"
local redis_port   = REDIS_PORT or 6379

--local FeatureAccessCodes= FeatureAccessCpdes


-- redis
local redis = require 'redis'
local client = redis.connect(redis_server, redis_port)
local response = client:ping()           -- true
print("notice", "connection to redis="..tostring(response).."\n")



-- raz bluebox data

--EVAL "for i, name in ipairs(redis:call('KEYS', 'bluebox:fax:*')) do redis.call('DEL', name); end"
--[[
keys= client:mget("bluebox*")
print("nb keys:".. #keys)
print("keys:"..tostring(keys))
for _,key in ipairs(keys) do
    print(key)
    end
--]]


-- inject fac into redis database

for name,content in pairs(FeatureAccessCodes) do

    --print(name,content)

    local code= content[1]
    local comment= content[2]

    local tag= string.sub(name,2,5)
    local operation= string.sub(name,1,1)

    --print(code,tag,operation,comment)

    local fac_key= 'bluebox:fac:'..code
    local fac_data= '"'..tag..";"..operation..";"..comment..'"'

    print ("redis set "..fac_key.." => "..fac_data)
    local r= client:set(fac_key,fac_data)

    --print("result: "..tostring(r))


    -- to test
    --client:set('/bluebox/account/1001/CFA','"+3315641"')
    --client:set('/bluebox/account/1002/CFR','"+3315641"')


end




