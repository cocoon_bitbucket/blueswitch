

uuid = require "uuid"
require("blueswitch_cfg")

local redis_server = REDIS_SERVER or "127.0.0.1"
local redis_port   = REDIS_PORT or 6379


-- get value from subscriber
subscriber_number = session:getVariable("accountcode"); -- or session:getVariable("caller_id_number"); 

-- get value from arguments
called_prefix= argv[1];
called_number= argv[2];



#local redis_server="127.0.0.1"


freeswitch.consoleLog("INFO", "Call from "..subscriber_number.." to "..called_number.."\n");



--[[ find the target number in cheching call forward reirection ]]--
function cfa_chain(target_num,db)

   -- redirect call if CFA
   local cf_record= "/bluebox/account/"..target_num.."/CFA"
   local cf_number= db:get(cf_record)  
   if cf_number then
   	    -- return the found route
   	    freeswitch.consoleLog("notice","found CFA for route:"..cf_record.." = "..tostring(cf_number).."\n")
   		return cf_number
   	else 
   		-- no CFA found
   		return nil
   end
end

--[[ find the target number in cheching call forward not reachable reirection ]]--
function cfnr_chain(target_num,db)

   -- redirect call if CFA
   local cf_record= "/bluebox/account/"..target_num.."/CFNR"
   local cf_number= db:get(cf_record)  
   if cf_number then
   	    -- a redirect on cfnr has been found to cf_number
   	    freeswitch.consoleLog("notice","found CFNR for route:"..cf_record.." = "..tostring(cf_number).."\n")
   	    -- check if no cfa for this number
   	    local cfa = cfa_chain( cf_number,db)
   	    if cfa then 
   			return cfa
   		else
   			return cf_number
   		end
   	else 
   		-- no CFA found
   		return nil
   end
end

--[[ find the target number in cheching call forward busy reirection ]]--
function cfb_chain(target_num,db)

   -- redirect call if CFA
   local cf_record= "/bluebox/account/"..target_num.."/CFB"
   local cf_number= db:get(cf_record)  
   if cf_number then
   	    -- a redirect on cfnr has been found to cf_number
   	    freeswitch.consoleLog("notice","found CFB for route:"..cf_record.." = "..tostring(cf_number).."\n")
   	    -- check if no cfa for this number
   	    local cfa = cfa_chain( cf_number,db)
   	    if cfa then 
   			return cfa
   		else
   			return cf_number
   		end
   	else 
   		-- no CF found
   		return nil
   end
end

--[[ find the target number in cheching call forward not answered ]]--
function cfna_chain(target_num,db)

   -- redirect call if CFA
   local cf_record= "/bluebox/account/"..target_num.."/CFNA"
   local cf_number= db:get(cf_record)  
   if cf_number then
   	    -- a redirect on cfnr has been found to cf_number
   	    freeswitch.consoleLog("notice","found CFNA for route:"..cf_record.." = "..tostring(cf_number).."\n")
   	    -- check if no cfa for this number
   	    local cfa = cfa_chain( cf_number,db)
   	    if cfa then 
   			return cfa
   		else
   			return cf_number
   		end
   	else 
   		-- no CF found
   		return nil
   end
end




-- number parts  +331465  + enterprise(1) + site (1) + value3)
--  eg           +331465       1              1     101      => + 33 1 46 51 1101
-- eg2           +331465       2              2     202      => + 33 1 46 52 2201

subscriber_enterprise= string.sub(subscriber_number,8,8)
subscriber_site= string.sub(subscriber_number,9,9)
subscriber_ident= string.sub(subscriber_number,10,12)

subscriber_extension = string.sub(subscriber_number,9,12)

freeswitch.consoleLog("INFO", "subscriber info e"..subscriber_enterprise.."/s"..subscriber_site.."\n")

-- redis 
local redis = require 'redis'
local client = redis.connect(redis_server, redis_port)
local response = client:ping()           -- true
freeswitch.consoleLog("INFO", "connection to redis="..tostring(response).."\n")


client:set('usr:nrk', 10)
client:set('usr:nobody', 5)
local value = client:get('/bluebox/myvar')      -- 

freeswitch.consoleLog("INFO", "/bluebox/myvar="..tostring(value).."\n")



-- Bridge LUA script for FreeSwitch for the PiPhone of LQDN
-- (C) digination / La Quadrature 2011-2013
-- distributed under GPL-3+ License.

local uuid = uuid()
dialnum1 = subscriber_number
dialnum2 = "+33"..called_number
--greeting_snd = "/usr/local/freeswitch/sounds/"..argv[4];

max_retriesl1 = 5;
max_retriesl2 = 3;
connected = false;
timeout = 45;

-- freeswitch.consoleLog("notice", "*********** STARTING Call ***********\n");
-- freeswitch.consoleLog("notice", "*********** DIALING "..dialnum1.." ***********\n");

-- originate_base1 = "{ignore_early_media=true,originate_timeout=90,hangup_after_bridge=true,origination_uuid="..uuid..",origination_caller_id_number=0970123456,leg=1}";
-- originate_str1 = originate_base1.."sofia/external/"..dialnum1.."@sip.tel.nnx.com";

-- session1 = null;
-- retries = 0;
-- ostr = "";
-- repeat  
--    retries = retries + 1;
--    freeswitch.consoleLog("notice", "*********** Dialing Leg1: " .. originate_str1 .. " - Try: "..retries.." ***********\n");
--    session1 = freeswitch.Session(originate_str1);
--    local hcause = session1:hangupCause();
--    freeswitch.consoleLog("notice", "*********** Leg1: " .. hcause .. " - Try: "..retries.." ***********\n");
-- until not ((hcause == 'NO_ROUTE_DESTINATION' or hcause == 'RECOVERY_ON_TIMER_EXPIRE' or hcause == 'INCOMPATIBLE_DESTINATION' or hcause == 'CALL_REJECTED' or hcause == 'NORMAL_TEMPORARY_FAILURE') and (retries < max_retriesl1))

session1=session


if (session1:ready()) then
   -- log to the console
   freeswitch.consoleLog("notice", "*********** Leg1  CONNECTED! ***********\n");

   -- Play greeting message
   -- if (not greeting_snd == "") then
   -- freeswitch.consoleLog("notice", "*********** Playing greeting sound: "..greeting_snd.." ***********\n");
   -- digits = session1:playAndGetDigits ( 1, 1, 1, "5000", "#", greeting_snd, "", "\\d")
   -- if (digits ~= "1") then
   --    freeswitch.consoleLog("notice", "*********** Leg1 NO 1 received, :"..digits..": cancelling ***********\n");
   --    session1:hangup();
   --    return;
   -- end
   -- end

   -- anonymus
    originate_base2 = "{ignore_early_media=true,originate_timeout=90,hangup_after_bridge=true,origination_uuid="..uuid..",origination_caller_id_number="..dialnum1..",leg=2,origination_caller_id_name=anonymous,sip_h_Privacy=id,privacy=yes}";

    -- display
    originate_base2 = "{ignore_early_media=true,originate_timeout=5,hangup_after_bridge=true,origination_uuid="..uuid..",origination_caller_id_number="..dialnum1..",leg=2,origination_caller_id_name="..dialnum1.."}";
 
	originate_str2 = originate_base2.."user/"..dialnum2;

   -- Set recording: uncomment these two lines if you'd like to record the call in stereo (one leg on each channel)
   --session1:setVariable("RECORD_STEREO", "true");
   --session1:execute("record_session", "/tmp/"..uuid..".wav");

   -- Set ringback
   session1:setVariable("ringback", "%(2000,4000,440,480)");

   -- redirect call if CFA
   local cfa_redirect = cfa_chain(dialnum2,client)
   if cfa_redirect then
    	freeswitch.consoleLog("info","session is redirected to "..tostring(cfa_redirect).."\n")
   		session1:transfer(cfa_redirect, "XML", "default")
   		return
   	end

   -- local cfa_record= "/bluebox/account/"..dialnum2.."/CFA"
   -- local cfa_number= client:get(cfa_record)  
   -- freeswitch.consoleLog("notice","CFA for session:"..cfa_record.." = "..tostring(cfa_number).."\n")
   -- if cfa_number then
   --  	freeswitch.consoleLog("info","session is redirected to "..tostring(cfa_number).."\n")
   -- 		session1:transfer(cfa_number, "XML", "default")
   -- 		return
   -- end


   retries = 0;
   session2 = null;
   repeat  
      -- Create session2
      retries = retries + 1;
      freeswitch.consoleLog("notice", "*********** Dialing: " .. originate_str2 .. " Try: "..retries.." ***********\n");
      session2 = freeswitch.Session(originate_str2, session1);
	  --session2 = freeswitch.Session("sofia/internal/"..dialnum2)

 	  local dispo2 = session2:getVariable("endpoint_disposition")                                                 
      local hcause = session2:hangupCause();
      freeswitch.consoleLog("INFO","session 2 disposition is '" ..dispo2.." hcause is "..hcause.."\n")
      freeswitch.consoleLog("notice", "*********** Leg2: " .. hcause .. " Try: " .. retries .. " ***********\n");

      if hcause == "USER_BUSY" then
      	-- check call forward on busy
      	local cfb_redirect = cfb_chain(dialnum2,client)
      	if cfb_redirect then
    		freeswitch.consoleLog("info","call busy ,session is redirected to "..tostring(cfb_redirect).."\n")
   			session1:transfer(cfb_redirect, "XML", "default")
      	end
      end
      if hcause == "NO_ANSWER" then
      	-- check call forward on no answer
      	local redirect = cfna_chain(dialnum2,client)
      	if redirect then
    		   freeswitch.consoleLog("info","no answer ,session is redirected to "..tostring(redirect).."\n")
   			session1:transfer(redirect, "XML", "default")
      	end
      end
      if hcause == "USER_NOT_REGISTERED" then
         -- check call forward on not reachable
         local redirect = cfnr_chain(dialnum2,client)
         if redirect then
            freeswitch.consoleLog("info","not registred ,session is redirected to "..tostring(redirect).."\n")
            session1:transfer(redirect, "XML", "default")
         end
      end
      if hcause == "CALL_REJECTED" or hcause == "ORIGINATOR_CANCEL" then
         -- check call forward on not reachable
         local redirect = cfnr_chain(dialnum2,client)
         if redirect then
            freeswitch.consoleLog("info","not reachable ,session is redirected to "..tostring(redirect).."\n")
            session1:transfer(redirect, "XML", "default")
         end
      end

   until not ((hcause == 'NO_ROUTE_DESTINATION' or hcause == 'RECOVERY_ON_TIMER_EXPIRE' or hcause == 'INCOMPATIBLE_DESTINATION' or hcause == 'CALL_REJECTED' or hcause == 'NORMAL_TEMPORARY_FAILURE') and (retries < max_retriesl2))

   if (session2:ready()) then
      freeswitch.consoleLog("notice", "*********** Leg2 (".. originate_str2 ..") CONNECTED! ***********\n");
      
      freeswitch.bridge(session1, session2);
      --bridge_session = freeswitch.bridge(session1, session2);
      -- while (session2:hangupState == false) do
      -- if (session2:answered) then
      -- DO_SOMETHING
      -- sleep(1);
      -- end
      -- end
      
      -- Hangup session2 if session1 is over
      if (session2:ready()) then session2:hangup(); end
   end
   -- hangup when done
   if (session1:ready()) then session1:hangup(); end
   
end
