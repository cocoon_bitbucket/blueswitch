--
-- fac.lua  handle bluebox feature access code activation and deactivation
--          fac.lua search fac info in redis database
--           /bluebox/fac/*2011   -> CFA;+;Call Forward Always activation
--          and set (+) or remove (-) an account fac entry
--           /bluebox/account/+33146511101/CFA  -> +3314651102

require("blueswitch_cfg")

local redis_server = REDIS_SERVER or "127.0.0.1"
local redis_port   = REDIS_PORT or 6379

-- get value from arguments
fac= argv[1]
number= argv[2]


--local redis_server="127.0.0.1"


if number== nil then number = "" end


-- get value from subscriber
subscriber_number = session:getVariable("accountcode")


freeswitch.consoleLog("INFO", "bluebox call from "..subscriber_number.." to "..fac..tostring(number).."\n")

-- redis 
local redis = require 'redis'
local client = redis.connect(redis_server, redis_port)
local response = client:ping()           -- true
freeswitch.consoleLog("notice", "connection to redis="..tostring(response).."\n")




-- search fac from redis db
--local fac= string.sub(fac,2,5)
local fac_key= "bluebox:fac:"..fac
freeswitch.consoleLog("notice", "fetch fac: "..fac_key.."\n")
local fac_entry= client:get(fac_key)
local fac_data={}
if fac_entry == nil then
	-- fac not found
	freeswitch.consoleLog("ERR", "no such a fac: "..fac_key.."\n")
else
	-- fac found , split it into name,action,comment
	freeswitch.consoleLog("notice", "found fac for "..fac..",entry="..fac_entry.."\n")
	local i=1
    for token in string.gmatch(fac_entry, "[^\;]+") do
    	freeswitch.consoleLog("debug", "token= "..token.."\n")
        fac_data[i] = token
        i = i+1
    end
    local fac_name = fac_data[1]
    local fac_action= fac_data[2]
    local fac_comment= fac_data[3]    
    freeswitch.consoleLog("notice", "fac name="..fac_name..",action="..fac_action.."comment="..fac_comment.."\n")

	local key= "/bluebox/account/"..subscriber_number.."/"..fac_name

    if fac_action == '+' then
    	-- activate feature access code
		freeswitch.consoleLog("INFO","fac activate "..fac_name.." with "..number.." for account "..subscriber_number.."\n")
		client:set(key, number)
	elseif fac_action == '-' then
		-- deactivate the feature access code
		freeswitch.consoleLog("INFO","fac Deactivate "..fac_name.." for account "..subscriber_number.."\n")
		client:del(key)
	else 
		-- unkown action
		freeswitch.consoleLog("ERR","ignore fac action"..fac_action.."for "..fac_name..","..fac_comment.."\n")
	end

	-- answer the call
	session:answer()

	-- sleep 1 second
	session:sleep(1000)

	-- play a file
	-- session:streamFile("/usr/local/freeswitch/sounds/music/8000/suite-espanola-op-47-leyenda.wav");

	-- hangup
	session:hangup()

end

 