-- blueswitch_config.lua

REDIS_SERVER="127.0.0.1"
REDIS_PORT=6379

FeatureAccessCodes =  {

    ["+CFA"]= { "*2011", "CFA Activation Call Forward Always"},
    ["+CFB"]= { "*2021", "CFB Activation , CallForward Busy Activation"},
    ["+CFNA"]= {"*2031","CFNA Activation , Call Forward No Answer Activation"},
    ["+CFNR"]= {"*2041", "CFNR Activation,    Call Forward Not Reachable Activation"},


    ["-CFA"]= {"*2010", "Call Forward Always Deactivation"},
    ["-CFB"]= {"*2020", "CFB DeActivation , CallForward Busy DeActivation"},
    ["-CFNA"]= {"*2030", "CFNA DeActivation , Call Forward No Answer DeActivation"},
    ["-CFNR"]= {"*2040", "CFNR DeActivation,    Call Forward Not Reachable DeActivation"},

    ["?CFA"]=  {"*2012", "Call Forward Always Interrogation"},
    ["?CFB"]=  {"*2022", "CFB Interrogation , CallForward Busy Interrogation"},
    ["?CFNA"]= {"*2032", "CFNA Interrogation , Call Forward No Answer Interrogation"},
    ["?CFNR"]= {"*2042", "CFNR Interrogation,    Call Forward Not Reachable Interrogation"},
}

