require("blueswitch_cfg")

local redis_server = REDIS_SERVER or "127.0.0.1"
local redis_port   = REDIS_PORT or 6379

local redis_url= REDIS_SERVER..":"..REDIS_PORT

local redis = require 'redis'
local client = redis.connect(redis_server, redis_port)
local response = client:ping()           -- true
print ("connection to redis server "..redis_url.." => "..tostring(response).."\n")


local value = client:get('/bluebox/myvar') 
print ("/bluebox/myvar="..tostring(value).."\n")