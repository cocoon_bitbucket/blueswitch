make a docker image based on freeswitch_vm
===========================================

# provisioning

  config.vm.provision "file", source: "./files/mod_lua_1.2.tar.gz", destination: "/tmp/mod_lua_1.2.tar.gz"
  config.vm.provision "file", source: "../freeswitch_conf/deploy/conf.zip", destination: "/tmp/conf.zip"
  config.vm.provision "file", source: "../freeswitch_conf/deploy/scripts.zip", destination: "/tmp/scripts.zip"
  #config.vm.provision "shell", path: "provision.sh"
  
  

files
=====



mod_lua_1.2.tar.gz
------------------

this file is the mod_lua found in freeswitch 1.2 and it is lua 5.1 (legacy)
this mod_lua to replace the mod_lua of freeswitch 1.6 ( which is lua 5.2)
   ( /usr/src/freeswitch._1.6.git/src/mod/languages/mod_lua )

to obtain this file:

    git clone -b v1.2 https://freeswitch.org/stash/scm/fs/freeswitch.git freeswitch_1.2.git

    cd freeswitch.1.2.git/src/mod/languages
    tar cvzf /tmp/mod_lua_1.2.tar.gz mod_lua

we get : /tmp/mod_lua_1.2.tar.gz
