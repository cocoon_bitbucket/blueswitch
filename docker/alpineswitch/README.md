note to use system wide lua libraries
see: https://wiki.freeswitch.org/wiki/Mod_lua#How_can_I_make_it_use_the_.22system_lua.22


#Q : Can I use luarocks with Freeswitch?

A : Yes - luarocks is a package/library manager for lua. It can install libraries into a systemwide directory 
    and then you can Freeswitch's LUA to use them. You may have to install a systemwide lua just to keep the 
    luarocks installer happy...
    
For example if you want the LuaXML library in Ubuntu 12.04:

    sudo apt-get install lua5.1 luarocks
    luarocks install luaxml
    
Then edit **/usr/local/freeswitch/conf/autoload_configs/lua.conf.xml**, de-comment lines to leave the following:

    <param name="module-directory" value="/usr/lib/lua/5.1/?.so"/>
    <param name="module-directory" value="/usr/local/lib/lua/5.1/?.so"/>
    
A lua script in your dialplan can now use the XML functions. A trivial example:
   require("LuaXml")
   local xobj = xml.eval('<Cmd Message="Hello"/>')
   freeswitch.consoleLog("INFO","The message in the XML is "..xobj["Message"].."\n")
How can I get Lua to see my own libraries using "require"

# Q: Can I use the require mechanism for including libraries with the Lua in FreeSWITCH?
A: You may need to alter the LUA_PATH variable to instruct the embedded Lua inside FreeSWITCH to find your libraries. A simple startup script to do this is:

    #!/bin/bash
    export LUA_PATH=/usr/local/freeswitch/scripts/?.lua\;\;
    ulimit -s 240
    screen /usr/local/freeswitch/bin/freeswitch
    
The default path is something like /usr/local/share/lua/5.1/

Another option for common code similar to the "include" directive in many languages is to use dofile
eg. dofile("/home/fred/scripts/fredLuaFunctions.lua")
Note that this will just execute the code contained in the file as if it were inline - this is not the same as creating a lua module