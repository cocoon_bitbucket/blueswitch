--
-- bluebox_call.lua  handle bluebox subscriber  1465 ? ????
---

-- get value from subscriber
subscriber_number = session:getVariable("accountcode") -- or session:getVariable("caller_id_number"); 
-- session:getVariable("caller_id_number"); 

-- get value from arguments
called_site_id= argv[1]
called_extension= argv[2]

-- constants
--bluebox_base= "+331465"
local redis_server="192.168.1.51"

-- number parts  +331465  + enterprise(1) + site (1) + value3)
--  eg           +331465       1              1     101      => + 33 1 46 51 1101
-- eg2           +331465       2              2     202      => + 33 1 46 52 2201

subscriber_enterprise= string.sub(subscriber_number,8,8)
subscriber_site= string.sub(subscriber_number,9,9)
subscriber_ident= string.sub(subscriber_number,10,12)
subscriber_extension = string.sub(subscriber_number,9,12)

subscriber_base_number= string.sub(subscriber_number,1,8)

freeswitch.consoleLog("INFO", "bluebox call from "..subscriber_number.." to "..called_extension.."\n")


freeswitch.consoleLog("INFO", "subscriber info e"..subscriber_enterprise.."/s"..subscriber_site.."\n")

-- redis 
local redis = require 'redis'
local client = redis.connect(redis_server, 6379)
local response = client:ping()           -- true
freeswitch.consoleLog("INFO", "connection to redis="..tostring(response).."\n")


client:set('usr:nrk', 10)
client:set('usr:nobody', 5)
local value = client:get('/bluebox/myvar')      -- 

freeswitch.consoleLog("INFO", "/bluebox/myvar="..value.."\n")

-- compute full number +3314650? + extension
called_number = subscriber_base_number..called_extension
-- transfer call to target

session:transfer( called_number, "XML", "default")
